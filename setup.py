from setuptools import setup

setup(name='eliza',
      version='1.0',
      description='Because the world needed another way of running ELIZA.',
      url='http://github.com/spwert/eliza',
      author='Stefan Wert',
      author_email='spwert@dipchipdip.net',
      license='The Unlicense',
      packages=['eliza'],
      zip_safe=False)
